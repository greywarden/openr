package pl.greywarden.openr.gui.favourite_programs;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ProgramWrapper {

    private String name;
    private String path;
    private String icon;

}
